package yahoo.domain;

public class Stock {

	private String symbol;
	private double lastTrade;
	private double open;
	
	public Stock(String symbol, double lastTrade, double close) {
		this.symbol = symbol;
		this.lastTrade = lastTrade;
		this.open = close;
	}

	public String getSymbol() {
		return symbol;
	}

	public double getLastTrade() {
		return lastTrade;
	}

	public double getOpen() {
		return open;
	}

	@Override
	public String toString() {
		return "Stock [symbol=" + symbol + ", lastTrade=" + lastTrade + ", open=" + open + "]";
	}
	
	
	
}
