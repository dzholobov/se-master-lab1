package yahoo;

import java.util.ArrayList;
import java.util.List;

import yahoo.domain.Stock;
import yahoo.web.YahooException;
import yahoo.web.YahooStockReader;

public class StockMarketMonitor {

	private List<Stock> currentStocks = new ArrayList<>();
	private List<YahooStockReader> readers = new ArrayList<>();
	
	public StockMarketMonitor(String[] symbols) {
		for (String symbol: symbols) {
			readers.add(new YahooStockReader(symbol));
		}
	}
	
	public void refresh() throws YahooException {
		currentStocks.clear();
		for (YahooStockReader reader: readers) {
			currentStocks.add(reader.getQuote());
		}
	}
	
	public void printData() {
		for (Stock stock: currentStocks) {
			System.out.println(stock);
		}
	}
	
}
