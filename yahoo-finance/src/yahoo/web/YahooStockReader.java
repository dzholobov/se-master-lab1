package yahoo.web;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import yahoo.domain.Stock;

// �� ������������� ���� �����!
public class YahooStockReader {

	private static final String YAHOO_URL_PATTERN = "http://download.finance.yahoo.com/d/quotes.csv?s=%s&f=sl1d1t1c1ohgv&e=.csv"; 

	private static final int SYMBOL = 0;
	private static final int LAST_TRADE = 1;
	private static final int OPEN = 5;
	
	private String symbol;

	public YahooStockReader(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbol() {
		return symbol;
	}
	
	public Stock getQuote() throws YahooException {
		String quoteURL = String.format(YAHOO_URL_PATTERN, symbol);
		
		Stock result = null;
		try {
			URL url = new URL(quoteURL);

			InputStream inputStream = url.openStream();
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(inputStream));
			String text = "";
			String line = bufferedReader.readLine();

			while (line != null) {
				text += line + "\n";
				line = bufferedReader.readLine();
			}
			bufferedReader.close();
			String fields[] = text.split(",");

			result = new Stock(fields[SYMBOL], Double.parseDouble(fields[LAST_TRADE]), Double.parseDouble(fields[OPEN]));

		} catch (Exception ex) {
			throw new YahooException("���������� �������� ������ �� ������: " + quoteURL);
		};

		return result;
		
	}
}
