package yahoo.web;

public class YahooException extends Exception {

	public YahooException(String message) {
		super(message);
	}
	
}
