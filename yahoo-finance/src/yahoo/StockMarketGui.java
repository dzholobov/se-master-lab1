package yahoo;

import java.util.Scanner;

import yahoo.web.YahooException;

public class StockMarketGui {

	private final static String[] STOCK_SYMBOLS = { "AAPL", "GOOG", "ORCL" };

	private StockMarketMonitor monitor;

	public StockMarketGui() {
		monitor = new StockMarketMonitor(STOCK_SYMBOLS);
	}

	public void start() {
		Scanner sc = new Scanner(System.in);
		String input = "";
		try {
			do {
				System.out.println("�������� ��������:");
				System.out.println("r - �������� ������");
				System.out.println("x - �����");
				input = sc.nextLine();
				if (input.equalsIgnoreCase("r")) {
					monitor.refresh();
					monitor.printData();
				}
			} while (!input.equalsIgnoreCase("x"));
		} catch (YahooException e) {
			System.out.println(e.getMessage());
		}
		sc.close();
	}

}
