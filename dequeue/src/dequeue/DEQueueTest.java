package dequeue;

import static org.junit.Assert.*;

import org.junit.Test;

public class DEQueueTest {

	@Test
	public void testIsEmptyAfterCreate() {
		DEQueue<String> dq = new DEQueue<>();
		assertEquals(0, dq.size());
	}
	
	@Test
	public void testPushBackOneString() {
		DEQueue<String> dq = new DEQueue<>();
		dq.pushBack("first");
		assertEquals(1, dq.size());
		assertEquals("first", dq.back());
	}

	// Напишите недостающие методы
	
}
